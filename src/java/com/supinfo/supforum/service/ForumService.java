/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.service;

import com.supinfo.supforum.dao.ForumDao;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class ForumService {
    
    @EJB
    ForumDao forumDao;

    public Date getCreatedDate(){
        return forumDao.getCreatedDate();
    }
    
}
