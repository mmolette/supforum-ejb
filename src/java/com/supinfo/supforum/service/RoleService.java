/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.service;

import com.supinfo.supforum.dao.RoleDao;
import com.supinfo.supforum.entity.Role;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class RoleService {
    
    @EJB
    RoleDao roleDao;

    public Role getRoleUser(){
        return roleDao.getRoleByID(Long.valueOf("0"));
    }
}
