/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.service;

import com.supinfo.supforum.dao.MessageDao;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class MessageService {
    
    @EJB
    MessageDao messageDao;

    public int getNbMessages(){
        return messageDao.getNbMessages();
    }
}
