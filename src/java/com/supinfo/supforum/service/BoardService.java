/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.service;

import com.supinfo.supforum.dao.BoardDao;
import com.supinfo.supforum.entity.Board;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Mathieu
 */
@Stateless
public class BoardService {
    
    @EJB
    BoardDao boardDao;
    
    public Board addBoard(Board board){
        
        boardDao.addBoard(board);
        return board;
    }
    
    public void removeBoard(Board board){
        
        boardDao.removeBoard(board);
    }
    
    public List<Board> getAllBoards(){
        return boardDao.getAllBoards();
    }
    
    public void removeBoardsById(List<Long> boards){
        
        boardDao.removeBoardsById(boards);
    }
    
    public void removeBoards(List<Board> boards){
        
        boardDao.removeBoards(boards);
    }
}
