/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.service;

import com.supinfo.supforum.dao.UserDao;
import com.supinfo.supforum.entity.User;
import com.supinfo.supforum.facade.UserFacade;
import com.supinfo.supforum.util.Hash;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class UserService {
    
    @EJB
    UserDao userDao;

    public User userExists(String username, String password) throws Exception{

        try{
            User user = userDao.getUserByUsername(username);
            Hash hash = new Hash();
            password = hash.hash("SHA1", password);

            if(user.getPassword().compareTo(password) != 0){
                user = null;
            }

            return user;
        }catch(Exception e){
            throw e;
        }
    }
    
    public User addUser(User user) throws Exception{
        
        try{
            userDao.addUser(user);
        }
        catch(Exception e){
            throw e;
        }
        return user;
    }
    
    public int getNbUsers(){
        return userDao.getNbUsers();
    }
}
