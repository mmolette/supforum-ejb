/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.service;

import com.supinfo.supforum.dao.CategoryDao;
import com.supinfo.supforum.entity.Category;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CategoryService {
    
    @EJB
    CategoryDao categoryDao;
    
    public Category addCategory(Category category){
        
        categoryDao.addCategory(category);
        return category;
    }
    
    public void removeCategory(Category category){
        
        categoryDao.removeCategory(category);
    }
    
    public void removeCategories(List<Long> categories){
        
        categoryDao.removeCategories(categories);
    }
    
    public Category getCategoryById(Long id){
        
        return categoryDao.getCategoryByID(id);
    }
    
    public void setCategory(Category category){
        
        categoryDao.setCategory(category);
    }

    public List<Category> getAllCategories(){
        
        return categoryDao.getAllCategories();
    }
}
