/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.entity.Forum;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface ForumDao {
    
    public Date getCreatedDate();
    /*
    public Forum addForum(Forum forum);
    public void removeForum(Forum forum); 
    public Forum getForumByID(Long id);
    */
}
