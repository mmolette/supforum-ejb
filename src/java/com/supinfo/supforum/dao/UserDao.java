/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.entity.Forum;
import com.supinfo.supforum.entity.Topic;
import com.supinfo.supforum.entity.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface UserDao {
    
    
    public User addUser(User user);
    public void removeUser(User user); 
    public User getUserByID(Long id);
    public int getNbUsers();
    /*
    public List<User> getUsersByForum(Forum forum);
    */
    public User getUserByUsername(String username) throws Exception;
}
