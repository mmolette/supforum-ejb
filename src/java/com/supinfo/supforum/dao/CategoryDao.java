/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.entity.Forum;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface CategoryDao {
    
    
    public Category addCategory(Category category);
    public void removeCategory(Category category); 
    public void removeCategories(List<Long> categories);
    public Category getCategoryByID(Long id);
    public void setCategory(Category category);
    //public List<Category> getCategoriesByForum(Forum forum);
    public List<Category> getAllCategories();
}
