/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.entity.Role;
import com.supinfo.supforum.entity.Topic;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface TopicDao {
   
    /*
    public Topic addTopic(Topic topic);
    public void removeTopic(Topic topic); 
    public Topic getTopicByID(Long id);
    */
    public List<Topic> getTopicsByBoard(Board board);
}
