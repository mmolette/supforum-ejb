/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Message;
import com.supinfo.supforum.entity.Role;
import com.supinfo.supforum.entity.Topic;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface RoleDao {
    
    /*
    public Role addRole(Role role);
    public void removeRole(Role role);*/ 
    public Role getRoleByID(Long id);
    /*
    public List<Role> getAllRoles();
    */
}
