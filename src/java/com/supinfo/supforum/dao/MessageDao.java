/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.entity.Forum;
import com.supinfo.supforum.entity.Message;
import com.supinfo.supforum.entity.Topic;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface MessageDao {
    
    /*
    public Message addMessage(Message message);
    public void removeMessage(Message message); 
    public Message getMessageByID(Long id);
    */
    public int getNbMessages();
    public List<Message> getMessagesByTopic(Topic topic);
}
