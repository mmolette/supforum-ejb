/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.dao;

import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.entity.Category;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface BoardDao {
    
    
    public Board addBoard(Board board);
    public void removeBoard(Board board);
    public void removeBoardsById(List<Long> boards);
    public void removeBoards(List<Board> boards);
    public Board getBoardByID(Long id);
    
    public List<Board> getBoardsByCategory(Category category);
    public List<Board> getAllBoards();
}
