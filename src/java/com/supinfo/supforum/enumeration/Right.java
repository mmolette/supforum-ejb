/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.enumeration;

/**
 *
 * @author Mathieu
 */
public enum Right {
    
    READ_MESSAGE, 
    WRITE_MESSAGE, 
    DELETE_MESSAGE, 
    LOCK_TOPIC, 
    MOVE_TOPIC, 
    MERGE_TOPIC, 
    DELETE_TOPIC, 
    WRITE_BOARD, 
    LOCK_BOARD, 
    DELETE_BOARD, 
    WRITE_CATEGORY, 
    DELETE_CATEGORY, 
    WRITE_PROFILE, 
    DELETE_PROFILE;
}
