/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.util;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Mathieu
 */
public class Email {

	Properties emailProperties;
	Session mailSession;
	MimeMessage emailMessage;
	 
	public Email() throws AddressException,MessagingException {	 
	 
	    setMailServerProperties();
	}
	 
	public void setMailServerProperties() {
	 
		// Port smtp
	    String emailPort = "587";
	 
	    emailProperties = System.getProperties();
	    emailProperties.put("mail.smtp.port", emailPort);
	    emailProperties.put("mail.smtp.auth", "true");
	    emailProperties.put("mail.smtp.starttls.enable", "true");
	 
	}
	 
	public void createEmailMessage(String[] toEmails, String subject, String message) throws AddressException, MessagingException {
 
	    mailSession = Session.getDefaultInstance(emailProperties, null);
	    emailMessage = new MimeMessage(mailSession);
	 
	    for (int i = 0; i < toEmails.length; i++) {
	      emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
	    }
	 
	    emailMessage.setSubject(subject);
	    emailMessage.setContent(message, "text/html");
	 
	}
	 
	public void sendEmail() throws AddressException, MessagingException {
	 
	    String emailHost = "smtp.gmail.com";
	    String fromUser = "159158supinfo@gmail.com";
	    String fromUserEmailPassword = "supinfo159158";
	 
	    Transport transport = mailSession.getTransport("smtp");
	 
	    transport.connect(emailHost, fromUser, fromUserEmailPassword);
	    transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
	    transport.close();
	}	 
}
