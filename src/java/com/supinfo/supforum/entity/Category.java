/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Mathieu
 */
@Entity
public class Category implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false, unique = true)
    private String name;
    
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Board> boards;
    
    /*
    @ManyToOne
    @JoinColumn(nullable = false)
    private Forum forum;
    */
    
    @Temporal(TemporalType.DATE)
    private Date created = new Date();
    
    @Temporal(TemporalType.DATE)
    private Date updated;
    
    public Category(){
        super();
    }
    
    /*
    public void basicSetForum(Forum myForum) {
        if (this.forum != myForum) {
            if (myForum != null){
                if (this.forum != myForum) {
                    Forum oldforum = this.forum;
                    this.forum = myForum;
                    if (oldforum != null)
                            oldforum.removeCategory(this);
                }
            }
        }	
    }
    */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Set<Board> getBoards() {
        if(this.boards == null) {
                        this.boards = new HashSet<Board>();
        }
        return (Set<Board>) this.boards;	
    }
    
    public void addAllBoards(Set<Board> newBoards) {
        if (this.boards == null) {
            this.boards = new HashSet<Board>();
        }
        for (Board tmp : newBoards)
            tmp.setCategory(this);

    }
    
    public void removeAllBoard(Set<Board> newBoards) {
        if(this.boards == null) {
                return;
        }

        this.boards.removeAll(newBoards);	
    }
    
    public void addBoard(Board newBoard) {
        if(this.boards == null) {
            this.boards = new HashSet<Board>();
        }

        if (this.boards.add(newBoard))
            newBoard.basicSetCategory(this);	
    }
    
    public void removeBoard(Board oldBoard) {
        if(this.boards == null)
            return;

        if (this.boards.remove(oldBoard))
            oldBoard.unsetCategory();

    }
    
    /*
    public Forum getForum() {
        return this.forum;	
    }
    
    public void setForum(Forum myForum) {
        this.basicSetForum(myForum);
        myForum.addCategory(this);	
    }
    
    public void unsetForum() {
        if (this.forum == null)
            return;
        Forum oldforum = this.forum;
        this.forum = null;
        oldforum.removeCategory(this);	
    }
    */
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Category)) {
            return false;
        }
        Category other = (Category) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.Category[ id=" + id + " ]";
    }
    
}
