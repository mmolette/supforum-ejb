/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Mathieu
 */
@Entity
public class Board implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false, unique = true)
    private String name;
    
    @Column(nullable = false)
    private String description;
    
    @ManyToOne(optional = false)
    @JoinColumn
    private Category category;
    
    @OneToMany(mappedBy = "board", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Topic> topics;
    
    @Temporal(TemporalType.DATE)
    private Date created = new Date();
    
    @Temporal(TemporalType.DATE)
    private Date updated;
    
    public Board(){
        super();
    }
    
    public void basicSetCategory(Category myCategory) {
        if (this.category != myCategory) {
            if (myCategory != null){
                if (this.category != myCategory) {
                    Category oldcategory = this.category;
                    this.category = myCategory;
                    if (oldcategory != null)
                        oldcategory.removeBoard(this);
                }
            }
        }	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Category getCategory() {
        return this.category;	
    }
    
    public void setCategory(Category myCategory) {
        this.basicSetCategory(myCategory);
        myCategory.addBoard(this);	
    }
    
    public void unsetCategory() {
        if (this.category == null)
            return;
        Category oldcategory = this.category;
        this.category = null;
        oldcategory.removeBoard(this);	
    }

    public Set<Topic> getTopics() {
        if(this.topics == null) {
            this.topics = new HashSet<Topic>();
        }
        return (Set<Topic>) this.topics;	
    }
    
    public void addAllTopics(Set<Topic> newTopics) {
        if (this.topics== null) {
            this.topics = new HashSet<Topic>();
        }
        for (Topic tmp : newTopics)
            tmp.setBoard(this);
			
    }
    
    public void removeAllTopics(Set<Topic> newTopics) {
        if(this.topics == null) {
            return;
        }

        this.topics.removeAll(newTopics);	
    }
    
    public void addTopic(Topic newTopic) {
        if(this.topics == null) {
            this.topics = new HashSet<Topic>();
        }

        if (this.topics.add(newTopic))
            newTopic.basicSetBoard(this);	
    }
    
    public void removeTopic(Topic oldTopic) {
        if(this.topics == null)
            return;

        if (this.topics.remove(oldTopic))
            oldTopic.unsetBoard();

    }
    
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Board)) {
            return false;
        }
        Board other = (Board) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.Board[ id=" + id + " ]";
    }
    
    
    @PreRemove
    public void preRemove() {
        System.out.println("preremove");
        this.unsetCategory();
    }
    
}
