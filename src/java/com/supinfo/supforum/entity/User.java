/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import com.supinfo.supforum.util.Hash;
import com.supinfo.supforum.enumeration.TypeEnum;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Mathieu
 */
@Entity
public class User implements Serializable {
        
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String username;
    
    @Column(nullable = false, unique = true)
    private String email;
    
    @Column(nullable = false)
    private String password;
    
    @OneToMany
    private Set<Message> messages;
    
    @Enumerated(EnumType.STRING)
    private TypeEnum role;
    /*
    @ManyToOne
    @JoinColumn(nullable = false)
    private Role role;
    */
    
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(nullable = false)
    private Date created;
    
    public User(){

    }
    
    public User(String username, String password, String email){
        this.email = email;
        this.username = username;
        this.role = TypeEnum.USER;
        this.created = new Date();

        Hash hash = new Hash();
        this.password = hash.hash("SHA1", password);
    }
    /*
    public void basicSetRole(Role myRole) {
        if (this.role != myRole) {
            if (myRole != null){
                if (this.role != myRole) {
                        Role oldrole = this.role;
                        this.role = myRole;
                        if (oldrole != null)
                                oldrole.removeUser(this);
                }
            }
        }	
    }
*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Message> getMessages() {
        if(this.messages == null) {
            this.messages = new HashSet<Message>();
        }
        return (Set<Message>) this.messages;	
    }

    public void addAllMessages(Set<Message> newMessages) {
        if (this.messages == null) {
            this.messages = new HashSet<Message>();
        }
        for (Message tmp : newMessages)
            tmp.setUser(this);
			
    }
	
    public void removeAllMessages(Set<Message> newMessages) {
        if(this.messages == null) {
                return;
        }

        this.messages.removeAll(newMessages);	
    }
    
    public void addMessage(Message newMessage) {
        if(this.messages == null) {
            this.messages = new HashSet<Message>();
        }

        if (this.messages.add(newMessage))
            newMessage.basicSetUser(this);	
    }
    
    public void removeMessage(Message oldMessage) {
        if(this.messages == null)
            return;

        if (this.messages.remove(oldMessage))
            oldMessage.unsetUser();

    }
    
    public TypeEnum getRole() {
        return this.role;	
    }
    /*
    public Role getRole() {
        return this.role;	
    }
    
    /*
    public void setRole(Role myRole) {
        this.basicSetRole(myRole);
        myRole.addUser(this);	
    }
    
    public void unsetRole() {
        if (this.role == null)
            return;
        Role oldrole = this.role;
        this.role = null;
        oldrole.removeUser(this);	
    }
    */
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.User[ id=" + id + " ]";
    }
    
}
