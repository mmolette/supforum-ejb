/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Mathieu
 */
@Entity
public class Topic implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = false)
    private Boolean locked = false;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Message> messages;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Board board;
    
    @Temporal(TemporalType.DATE)
    private Date creadted;
    
    @Temporal(TemporalType.DATE)
    private Date updated;
    
    public Topic(){
        super();
    }
    
    public void basicSetBoard(Board myBoard) {
        if (this.board != myBoard) {
            if (myBoard != null){
                if (this.board != myBoard) {
                    Board oldboard = this.board;
                    this.board = myBoard;
                    if (oldboard != null)
                        oldboard.removeTopic(this);
                }
            }
        }	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
    
    public Set<Message> getMessage() {
        if(this.messages == null) {
            this.messages = new HashSet<Message>();
        }
        return (Set<Message>) this.messages;	
    }
    
    public void addAllMessage(Set<Message> newMessages) {
        if (this.messages == null) {
            this.messages = new HashSet<Message>();
        }
        for (Message tmp : newMessages)
            tmp.setTopic(this);

    }
    
    public void removeAllMessage(Set<Message> newMessages) {
        if(this.messages == null) {
                return;
        }

        this.messages.removeAll(newMessages);	
    }
    
    public void addMessage(Message newMessage) {
        if(this.messages == null) {
            this.messages = new HashSet<Message>();
        }

        if (this.messages.add(newMessage))
            newMessage.basicSetTopic(this);	
    }
    
    public void removeMessage(Message oldMessage) {
        if(this.messages == null)
            return;

        if (this.messages.remove(oldMessage))
            oldMessage.unsetTopic();

    }
    
    public Board getBoard() {
        return this.board;	
    }
    
    public void setBoard(Board myBoard) {
        this.basicSetBoard(myBoard);
        myBoard.addTopic(this);	
    }
    
    public void unsetBoard() {
        if (this.board == null)
            return;
        Board oldboard = this.board;
        this.board = null;
        oldboard.removeTopic(this);	
    }

    public Date getCreadted() {
        return creadted;
    }

    public void setCreadted(Date creadted) {
        this.creadted = creadted;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Topic)) {
            return false;
        }
        Topic other = (Topic) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.Topic[ id=" + id + " ]";
    }
    
}
