/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import com.supinfo.supforum.enumeration.Right;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Mathieu
 */
@Entity
public class Role implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = Right.class)
    private Set<Right> rights;
    
    /*
    @OneToMany(mappedBy = "role")
    private Set<User> users;
    */
    
    public Role(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Right> getRights() {
        return rights;
    }

    public void setRights(Set<Right> rights) {
        this.rights = rights;
    }
    
    public void addAllRights(Set<Right> newRights) {
        if (this.rights == null) {
                this.rights = new HashSet<Right>();
        }
        this.rights.addAll(newRights);	
    }
    
    public void removeAllRights(Set<Right> newRights) {
        if(this.rights == null) {
            return;
        }

        this.rights.removeAll(newRights);	
    }
    
    public void addRight(Right newRight) {
        if(this.rights == null) {
            this.rights = new HashSet<Right>();
        }

        this.rights.add(newRight);	
    }
    
    private void removeRight(Right oldRight) {
        if(this.rights == null)
            return;

        this.rights.remove(oldRight);	
    }

    /*
    public Set<User> getUsers() {
        if(this.users == null) {
                        this.users = new HashSet<User>();
        }
        return (Set<User>) this.users;	
    }
    /*
    public void addAllUser(Set<User> newUsers) {
        if (this.users == null) {
            this.users = new HashSet<User>();
        }
        for (User tmp : newUsers)
            tmp.setRole(this);
    }
    */
    /*
    public void removeAllUser(Set<User> newUser) {
        if(this.users == null) {
            return;
        }

        this.users.removeAll(newUser);	
    }
    
    /*
    public void addUser(User newUser) {
        if(this.users == null) {
            this.users = new HashSet<User>();
        }

        if (this.users.add(newUser))
            newUser.basicSetRole(this);	
    }
    /*
    public void removeUser(User oldUser) {
        if(this.users == null)
            return;

        if (this.users.remove(oldUser))
            oldUser.unsetRole();

    }
    */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.Role[ id=" + id + " ]";
    }
    
}
