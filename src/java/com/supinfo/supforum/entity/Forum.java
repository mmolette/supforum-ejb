/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Mathieu
 */
@Entity
public class Forum implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = false)
    private String description;
    
    @OneToMany
    private Set<Category> categories;
    
    @Temporal(TemporalType.DATE)
    private Date created;
    
    public Forum(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Set<Category> getCategories() {
        if(this.categories == null) {
            this.categories = new HashSet<Category>();
        }
        return (Set<Category>) this.categories;	
    }
    
    /*
    public void addAllCategories(Set<Category> newCategories) {
        if (this.categories == null) {
            this.categories = new HashSet<Category>();
        }
        for (Category tmp : newCategories)
            tmp.setForum(this);

    }
    */
    public void removeAllCategories(Set<Category> newCategories) {
        if(this.categories == null) {
                return;
        }

        this.categories.removeAll(newCategories);	
    }
    
    /*
    public void addCategory(Category newCategory) {
        if(this.categories == null) {
            this.categories = new HashSet<Category>();
        }

        if (this.categories.add(newCategory))
            newCategory.basicSetForum(this);	
    }
    
    public void removeCategory(Category oldCategory) {
        if(this.categories == null)
            return;

        if (this.categories.remove(oldCategory))
            oldCategory.unsetForum();

    }
    */
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Forum)) {
            return false;
        }
        Forum other = (Forum) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.Forum[ id=" + id + " ]";
    }
    
}
