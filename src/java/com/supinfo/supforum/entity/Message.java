/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Mathieu
 */
@Entity
public class Message implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String text;
    
    @Column(nullable = false)
    private Boolean first = false;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Topic topic;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;
    
    @Temporal(TemporalType.DATE)
    private Date created;
    
    @Temporal(TemporalType.DATE)
    private Date updated;
    
    public Message(){
        super();
    }
    
    public void basicSetTopic(Topic myTopic) {
        if (this.topic != myTopic) {
            if (myTopic != null){
                if (this.topic != myTopic) {
                    Topic oldtopic = this.topic;
                    this.topic = myTopic;
                    if (oldtopic != null)
                        oldtopic.removeMessage(this);
                }
            }
        }	
    }
    
    public void basicSetUser(User myUser) {
        if (this.user != myUser) {
            if (myUser != null){
                if (this.user != myUser) {
                    User olduser = this.user;
                    this.user = myUser;
                    if (olduser != null)
                        olduser.removeMessage(this);
                }
            }
        }	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }
    
    public Topic getTopic() {
        return this.topic;	
    }
    
    public void setTopic(Topic myTopic) {
        this.basicSetTopic(myTopic);
        myTopic.addMessage(this);	
    }
    
    public void unsetTopic() {
        if (this.topic == null)
            return;
        Topic oldtopic = this.topic;
        this.topic = null;
        oldtopic.removeMessage(this);	
    }
    
    public User getUser() {
        return this.user;	
    }
    
    public void setUser(User myUser) {
        this.basicSetUser(myUser);
        myUser.addMessage(this);	
    }
    
    public void unsetUser() {
        if (this.user == null)
            return;
        User olduser = this.user;
        this.user = null;
        olduser.removeMessage(this);
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Message)) {
            return false;
        }
        Message other = (Message) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.supforum.entity.Message[ id=" + id + " ]";
    }
    
}
