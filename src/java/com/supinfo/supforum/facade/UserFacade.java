/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.UserDao;
import com.supinfo.supforum.entity.Forum;
import com.supinfo.supforum.entity.Topic;
import com.supinfo.supforum.entity.Topic_;
import com.supinfo.supforum.entity.User;
import com.supinfo.supforum.entity.User_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public User getUserByUsername(String username) throws Exception{
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(user.get(User_.username), username));
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        
        try{
            User u = em.createQuery(query).getSingleResult();
            
            return u;
        }catch(PersistenceException pe){
            throw new Exception("This account doesn't exist !", pe.getCause());
        }
    }

    @Override
    public User addUser(User user) {
        this.create(user);
        return user;
    }

    @Override
    public void removeUser(User user) {
        this.remove(user);
    }

    @Override
    public User getUserByID(Long id) {
        return this.find(id);
    }

    @Override
    public int getNbUsers() {
        return this.count();
    }
}
