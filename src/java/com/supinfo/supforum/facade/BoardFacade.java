/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.BoardDao;
import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.entity.Board_;
import com.supinfo.supforum.entity.Category;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class BoardFacade extends AbstractFacade<Board> implements BoardDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BoardFacade() {
        super(Board.class);
    }


    @Override
    public List<Board> getBoardsByCategory(Category category) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Board> query = cb.createQuery(Board.class);
        Root<Board> board = query.from(Board.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(board.get(Board_.category), category));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
    }

    @Override
    public Board addBoard(Board board) {
        
        this.create(board);
        return board;
    }

    @Override
    public void removeBoard(Board board) {
        this.remove(board);
    }

    @Override
    public Board getBoardByID(Long id) {
        
        return this.find(id);
    }

    @Override
    public List<Board> getAllBoards() {
        return this.findAll();
    }
    
    @Override
    public void removeBoardsById(List<Long> boards) {
        /*
        Query q = em.createQuery("delete from Board C where C.id in :ids");
        q.setParameter("ids", boards);
        System.out.println(q.toString());
        q.executeUpdate();
                */
    }
    
    @Override
    public void removeBoards(List<Board> boards) {
        /*
        Query q = em.createQuery("delete from Board C where C.id in :ids");
        q.setParameter("ids", boards);
        System.out.println(q.toString());
        q.executeUpdate();
                */
        for (Board board : boards) {
            //board.unsetCategory();
            this.remove(board);
        }
    }
    
}
