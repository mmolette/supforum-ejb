/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.TopicDao;
import com.supinfo.supforum.entity.Board;
import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.entity.Category_;
import com.supinfo.supforum.entity.Topic;
import com.supinfo.supforum.entity.Topic_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class TopicFacade extends AbstractFacade<Topic> implements TopicDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TopicFacade() {
        super(Topic.class);
    }

    @Override
    public List<Topic> getTopicsByBoard(Board board) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Topic> query = cb.createQuery(Topic.class);
        Root<Topic> topic = query.from(Topic.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(topic.get(Topic_.board), board));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
    }
    
}
