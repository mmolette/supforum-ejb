/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.RoleDao;
import com.supinfo.supforum.entity.Role;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Mathieu
 */
@Stateless
public class RoleFacade extends AbstractFacade<Role> implements RoleDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RoleFacade() {
        super(Role.class);
    }

    @Override
    public Role getRoleByID(Long id) {
        return this.find(id);
    }
    
}
