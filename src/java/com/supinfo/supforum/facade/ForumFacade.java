/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.ForumDao;
import com.supinfo.supforum.entity.Forum;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mathieu
 */
@Stateless
public class ForumFacade extends AbstractFacade<Forum> implements ForumDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ForumFacade() {
        super(Forum.class);
    }
    
    @Override
    public Date getCreatedDate(){
        
        Query query = em.createQuery("SELECT MIN(U.created) FROM User as U ");
        
        return (Date) query.getSingleResult();
    }
}
