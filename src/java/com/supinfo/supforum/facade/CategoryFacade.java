/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.CategoryDao;
import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.entity.Category_;
import com.supinfo.supforum.entity.Forum;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CategoryFacade extends AbstractFacade<Category> implements CategoryDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }

    /*
    @Override
    public List<Category> getCategoriesByForum(Forum forum) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        Root<Category> category = query.from(Category.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(category.get(Category_.forum), forum));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
    }
    */

    @Override
    public Category addCategory(Category category) {
        this.create(category);
        return category;
    }

    @Override
    public void removeCategory(Category category) {
        this.remove(category);
    }

    @Override
    public Category getCategoryByID(Long id) {
        return this.find(id);
    }

    @Override
    public List<Category> getAllCategories() {
        return this.findAll();
    }

    @Override
    public void setCategory(Category category) {
        this.edit(category);
    }

    @Override
    public void removeCategories(List<Long> categories) {
        Query q = em.createQuery("delete from Category C where C.id in :ids");
        q.setParameter("ids", categories);
        System.out.println(q.toString());
        q.executeUpdate();
    }
}
