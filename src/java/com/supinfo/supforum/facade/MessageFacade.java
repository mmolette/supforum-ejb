/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.supforum.facade;

import com.supinfo.supforum.dao.MessageDao;
import com.supinfo.supforum.entity.Category;
import com.supinfo.supforum.entity.Category_;
import com.supinfo.supforum.entity.Message;
import com.supinfo.supforum.entity.Message_;
import com.supinfo.supforum.entity.Topic;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class MessageFacade extends AbstractFacade<Message> implements MessageDao{
    
    @PersistenceContext(unitName = "SupForum-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MessageFacade() {
        super(Message.class);
    }

    @Override
    public List<Message> getMessagesByTopic(Topic topic) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Message> query = cb.createQuery(Message.class);
        Root<Message> message = query.from(Message.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(message.get(Message_.topic), topic));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
    }

    @Override
    public int getNbMessages() {
        return this.count();
    }
    
}
